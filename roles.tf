resource "aws_iam_role" "lambda-api" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  tags = {
    Name = var.lambda__api__iam_role_tag_name
  }
}

resource "aws_iam_role_policy_attachment" "lambda-function-cloudwatch" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  role = aws_iam_role.lambda-api.name

  depends_on = [aws_iam_role.lambda-api]
}

resource "aws_iam_role_policy_attachment" "api-gateway-cloudwatch" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
  role = aws_iam_role.lambda-api.name

  depends_on = [aws_iam_role.lambda-api]
}

resource "aws_iam_role_policy_attachment" "api-gateway-xray" {
  policy_arn = "arn:aws:iam::aws:policy/AWSXrayFullAccess"
  role = aws_iam_role.lambda-api.name

  depends_on = [aws_iam_role.lambda-api]
}

resource "aws_iam_role_policy_attachment" "lambda-api-s3" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role = aws_iam_role.lambda-api.name

  depends_on = [aws_iam_role.lambda-api]
}

resource "aws_iam_role_policy" "lambda-api-allow-ec2-create-eni" {
  role = aws_iam_role.lambda-api.id
  depends_on = [aws_iam_role.lambda-api]
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda-api-allow-send-mail" {
  role = aws_iam_role.lambda-api.id
  depends_on = [aws_iam_role.lambda-api]
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ses:SendRawEmail"
      ],
      "Effect": "Allow",
      "Resource": "${var.lambda__api__security_group_ses_role_ses_identity_arn}"
    }
  ]
}
EOF
}