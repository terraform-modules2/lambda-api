output "lambda-security-group-id" {
  value = aws_security_group.lambda-sg.id
}

output "api-gateway-id" {
  value = aws_api_gateway_rest_api.lambda-api.id
}

output "root-resource-id" {
  value = aws_api_gateway_rest_api.lambda-api.root_resource_id
}

output "function-name" {
  value = aws_lambda_function.lambda-api.function_name
}

output "api-gateway-execution-arn" {
  value = aws_api_gateway_rest_api.lambda-api.execution_arn
}

output "function-arn" {
  value = aws_lambda_function.lambda-api.arn
}

output "function-invoke-arn" {
  value = aws_lambda_function.lambda-api.invoke_arn
}

output "lambda-role-arn" {
  value = aws_iam_role.lambda-api.arn
}