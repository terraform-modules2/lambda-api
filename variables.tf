variable "networking_group_tag" {type = string}
variable "compute_group_tag" {type = string}
variable "security_group_tag" {type = string}

variable "lambda__api__apig_binary_media_types" {type = list(string)}
variable "lambda__api__apig_name" {type = string}
variable "lambda__api__apig_description" {type = string}

variable "lambda__api__security_group_ses_role_ses_identity_arn" {type = string}
variable "lambda__api__security_group_vpc_id" {type = string}

variable "lambda__api__func_function_name" {type = string}
variable "lambda__api__func_function_handler" {type = string}
variable "lambda__api__func_function_timeout" {type = number}
variable "lambda__api__func_function_memory_allocation" {type = number}
variable "lambda__api__func_layers" {type = list(string)}
variable "lambda__api__func_vpc_subnet_ids" {type = list(string)}
variable "lambda__api__func_environment_variables_map" {}

variable "lambda__api__iam_role_tag_name" {type = string}
variable "lambda__api__security_group_tag_name" {type = string}
