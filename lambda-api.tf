resource "aws_lambda_function" "lambda-api" {
  function_name = var.lambda__api__func_function_name
  handler = var.lambda__api__func_function_handler
  role = aws_iam_role.lambda-api.arn
  runtime = "provided"

  vpc_config {
    security_group_ids = [aws_security_group.lambda-sg.id]
    subnet_ids = var.lambda__api__func_vpc_subnet_ids
  }

  filename = data.archive_file.lambda_dummy.output_path

  timeout = var.lambda__api__func_function_timeout
  memory_size = var.lambda__api__func_function_memory_allocation

  environment {
    variables = var.lambda__api__func_environment_variables_map
  }

  tracing_config {
    mode = "Active"
  }

  layers = var.lambda__api__func_layers
  publish = true

  tags = {
    group = var.compute_group_tag
  }

  depends_on = [
    aws_iam_role.lambda-api,
    aws_security_group.lambda-sg
  ]
}

resource "aws_lambda_permission" "lambda-api-perm" {
  statement_id = "AllowExecutionFromApiGateway"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda-api.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.lambda-api.execution_arn}/*/*"

  depends_on = [
    aws_lambda_function.lambda-api,
    aws_api_gateway_rest_api.lambda-api
  ]
}

data "archive_file" "lambda_dummy" {
  output_path = "./archive/dummy.zip"
  type = "zip"

  source {
    content = "lambda"
    filename = "lambda-dummy.txt"
  }
}