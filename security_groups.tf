resource "aws_security_group" "lambda-sg" {
  vpc_id = var.lambda__api__security_group_vpc_id

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.lambda__api__security_group_tag_name
    group = var.security_group_tag
  }
}