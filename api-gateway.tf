resource "aws_api_gateway_rest_api" "lambda-api" {
  name = var.lambda__api__apig_name
  description = var.lambda__api__apig_description

  binary_media_types = var.lambda__api__apig_binary_media_types

  tags = {
    group = var.networking_group_tag
  }
}

